<?php defined('CORE_ROOT') or die('access denied'); ?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <base href="<?php echo $base_url;?>" target="self"/>
    <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script data-main='assets/js/bootstrap' src='assets/core/require/require.js?v=1.062' data-cache="false"></script>
  </head>
  <body>
    <div class="wrapper">
      <div class="content container-fluid">
        <div class="loader"></div>
      </div>
    </div>
  </body>
</html>

<script type="text/ajs" data-view="login/form">
  <div class="row col-sm-6 col-sm-offset-3 col-lg-offset-4 col-lg-4 padding">
    <div class="form-group row">
      <label for="login" class="col-sm-2 control-label">Login</label>
      <div class="col-sm-10">
        <input name="login" id="login" type="text" class="form-control" placeholder="login">
        <div class="error-login"></div>
      </div>
    </div>
    <div class="form-group row">
      <label for="password" class="col-sm-2 control-label">Password</label>
      <div class="col-sm-10">
        <input name="password" id="password" type="password" class="form-control" placeholder="password">
        <div class="error-password"></div>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-offset-2 col-sm-10">
        <button class="btn btn-primary" id="submit">Login</button>
        <button class="btn btn-default" id="register">Register</button>
      </div>
    </div>
  </div>
</script>

<script type="text/ajs" data-view="register/form">
  <div class="row col-sm-6 col-sm-offset-3 col-lg-offset-4 col-lg-4 padding">
    <div class="form-group row">
      <label for="login" class="col-sm-2 control-label">Login</label>
      <div class="col-sm-10">
        <input name="login" id="login" type="text" class="form-control" placeholder="login">
        <div class="error-login"></div>
      </div>
    </div>
    <div class="form-group row">
      <label for="password" class="col-sm-2 control-label">Password</label>
      <div class="col-sm-10">
        <input name="password" id="password" type="password" class="form-control" placeholder="password">
        <div class="error-password"></div>
      </div>
    </div>
    <div class="form-group row">
      <label for="repassword" class="col-sm-2 control-label">Re-enter password</label>
      <div class="col-sm-10">
        <input name="repassword" id="repassword" type="password" class="form-control" placeholder="password">
        <div class="error-repassword"></div>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-offset-2 col-sm-10">
        <button class="btn btn-primary" id="submit">Add</button>
      </div>
    </div>
  </div>
</script>

<script type="text/ajs" data-view="chat/form">
  <div class="row col-sm-12">
    <div class="col-sm-10 left">
      <div class="chat">
        {{content}}
      </div>
      <div class="message">
        <textarea name="message" id="message"></textarea>
        <button id="send" class="btn btn-primary">send</button>
      </div>
    </div>
    <div class="col-sm-2 right">
      <div class="list">
        {{list}}
      </div>
    </div>
  </div>
</script>

<script type="text/ajs" data-view="chat/list">
  <p>{{user}}</p>
</script>

<script type="text/ajs" data-view="chat/message">
  <div class="row">
    <div class="col-sm-1">
      {{name}}:
    </div>
    <div class="col-sm-11">
      {{message}}
    </div>
  </div>
</script>
