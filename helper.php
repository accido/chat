<?php return array (
  'server' => 
  array (
    'listen' => '8080',
    'timeout' => 17,
    'ssl' => false,
    'host' => '127.5.187.1',
    'name' => '127.5.187.1',
    'memory_limit' => '512M',
    'async' => true,
    'worker' => 1,
  ),
  'application' => 
  array (
    'charset' => 'UTF-8',
    'base_url' => 'http://chat-accido.rhcloud.com/',
    'language' => NULL,
  ),
  'log' => 
  array (
    'type' => 'core',
    'display' => 1,
    'level' => 1,
    'core' => 
    array (
      'enable' => 1,
      'html' => 0,
      'stream' => '/logs/core.log',
    ),
    'phpconsole' => 
    array (
      'password' => 'root',
      'charset' => 'UTF-8',
      'ssl_only' => false,
      'allowed_ip_mask' => 
      array (
      ),
      'detect_trace_and_source' => true,
      'eval' => 
      array (
        'enable' => true,
        'shared_vars' => 
        array (
          'post' => '_POST',
        ),
        'base_dir' => 
        array (
          0 => '/var/www/chat/www',
        ),
      ),
      'handler' => 
      array (
        'disable_olds' => false,
        'disable_errors' => false,
        'disable_exceptions' => false,
      ),
    ),
  ),
  'router' => 
  array (
    'live' => false,
  ),
);
