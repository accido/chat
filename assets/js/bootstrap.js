define('configCore', function(){
  return {
    paths                                           : {
      app                                           : '../js'
    },
    baseUrl                                         : {
      auto                                          : true,
      src                                           : '127.0.0.1'
    },
    controllers                                     : {
      excludeSearch                                 : [
        'application/run',
        'main',
        'login',
        'register',
        'chat'
      ]
    }
  };
});

//require(['../core/bootstrap'])
require(['assets/core/bootstrap.min.js'])

define('ready', ['model', 'controller', 'view', 'option'], function(Model, Controller, View, Option){
  Model.register('Error');
  Model.register('Chat');
try{
  Model.handle('application/run').then(Model.register('Action/Init/Router'))
}
catch(error){
  console.log(error)
}
  Model.handle('/').then(function(ctrl){
    ctrl.commit()
      .then(function(ctrl){
        ctrl.execute('login');
        return ctrl;
      });
    return ctrl;
  });

  Model.handle('/login').then(function(ctrl){
    ctrl.commit()
    .then(function(ctrl){
      ctrl._view = View.factory('login');
      return ctrl;
    });
    return ctrl;
  })
  .then(Model.register('Render/View'));

  Model.handle('/register').then(function(ctrl){
    ctrl.commit()
    .then(function(ctrl){
      ctrl._view = View.factory('register');
      return ctrl;
    });
    return ctrl;
  })
  .then(Model.register('Render/View'));

  Model.handle('/chat').then(function(ctrl){
    ctrl.commit()
    .then(function(ctrl){
      ctrl._view = View.factory('chat');
      return ctrl;
    });
    return ctrl;
  })
  .then(Model.register('Render/View'));

  Controller.execute('application/run');
});
