<style>
  .error-frame .error-modal{
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: 0.1;
    background-color: #333;
  }

  .error-frame .error-message{
    width: 400px;
    height: 200px;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    background-color: #fff;
    border-radius: 5px;
  }

  .error-frame .error-message .error-message-header{
    padding: 1px 10px;
    background-color: #3276B1;
    border-radius: 5px 5px 0 0;
    height: 50px;
    color: #FFF;
  }

  .error-frame .error-message .error-message-content{
    padding: 5px;
    background-color: transparent;
    height: 100px;
  }

  .error-frame .error-message .error-message-nav{
    height: 50px;
  }

  .error-frame .error-message .error-message-nav .close-button{
    color: #FFF;
    background-color: #3276B1;
    border-color: #285E8E;
    text-decoration: none;
    outline: thin dotted;
    outline-offset: -2px;
    display: inline-block;
    margin-bottom: 0px;
    font-weight: 400;
    text-align: center;
    vertical-align: middle;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857;
    border-radius: 4px;
    -moz-user-select: none;
    font-family: inherit;
    text-transform: none;
    overflow: visible;
    font: inherit;
    margin: 0px;
    box-sizing: border-box;
    float: right;
    margin: 0 20px;
  }
</style>
<div class="error-frame">
  <div class="error-modal"></div>
  <div class="error-message">
    <div class="error-message-header">
      <h4 class="error-message-title">
        {{title}}
      </h4>
      <div class="close-icon-button"></div>
    </div>
    <div class="error-message-content">
      {{content}}
    </div>
    <div class="error-message-nav">
      <button class="close-button">Ok</button>
    </div>
  </div>
</div>
