var $ = document.querySelectorAll.bind(document),
    $content = $('.content')[0];

Model.register('Node/Dependency', $content, 'register').then(function(dep){
  return Model.register('Render/Node', dep);
})
.then(function(node){
  var form = view.factory('register/form');
  return node.render(form)
})  
.listen(Model.register('Socket'))
.listen(Model.register('Render/Dom', $content))
.then(function(data, socket, dom){
  dom.render(data);
  var $login = dom.search('#login'),
      $pass = dom.search('#password'),
      $repass = dom.search('#repassword'),
      $add = dom.search('#submit');

  view.asEventStream('login').alias('rlogin').send(socket);
  view.asEventStream('password').alias('rpassword').send(socket);
  view.asEventStream('repassword').alias('repassword').send(socket);
  view.asEventStream('submit').alias('rsubmit').send(socket);

  $login.onchange = function(e){
    e.preventDefault();
    view.set('login', this.value)
  };

  $pass.onchange = function(e){
    e.preventDefault();
    view.set('password', this.value)
  };

  $repass.onchange = function(e){
    e.preventDefault();
    view.set('repassword', this.value)
  };

  $add.onclick = function(e){
    e.preventDefault();
    view.set('submit', [view.get('login'), view.get('password')]);
    Controller.execute('login');
  };
})
.fail(function(error){
  console.log(error);
});
