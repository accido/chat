var $ = document.querySelectorAll.bind(document),
    $content = $('.content')[0];

Model.register('Node/Dependency', $content, 'login').then(function(dep){
  return Model.register('Render/Node', dep);
})
.then(function(node){
  var form = view.factory('login/form');
  return node.render(form)
})
.listen(Model.register('Socket'))
.listen(Model.register('Render/Dom', $content))
.then(function(data, socket, dom){
  dom.render(data);
  var $login = dom.search('#login'),
      $pass = dom.search('#password'),
      $reg = dom.search('#register'),
      $submit = dom.search('#submit');
  
  view.asEventStream('login').send(socket);
  view.asEventStream('password').send(socket);
  view.asEventStream('submit').send(socket);
  view.asEventStream('user')
  .recv(socket)
  .then(function(data){
    Controller.execute('chat');
  });

  $reg.onclick = function(e){
    e.preventDefault();
    Controller.execute('register');
  };

  $login.onchange = function(e){
    e.preventDefault();
    view.set('login', this.value)
  };

  $pass.onchange = function(e){
    e.preventDefault();
    view.set('password', this.value)
  };

  $submit.onclick = function(e){
    e.preventDefault();
    view.set('submit', {
      login : view.get('login'), 
      password : view.get('password')
    });
  }
})
.fail(function(error){
  console.log(error);
});
