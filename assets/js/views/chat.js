var $ = document.querySelectorAll.bind(document),
    $content = $('.content')[0];

Model.register('Node/Dependency', $content, 'chat').then(function(dep){
  return Model.register('Render/Node', dep);
})
.listen(Model.register('chat'))
.then(function(node, chat){
  var form = view.factory('chat/form');

  form.set('content', 'no messages');
  form.set('list', 'no users');
  chat.asEventStream('VAR_CHAT').then(function(data){
    var mview, index, stream = new Option(data);
    for(index in data){
      mview = view.factory('chat/message');
      mview.set('name', data[index].name);
      mview.set('message', data[index].message);
      stream.listen(node.render(mview));
    }
    stream.apply(false, function(messages){
      form.set('content', messages);
    });
  });

  chat.asEventStream('VAR_LIST').then(function(list){
    var lview, index, stream = new Option(list);
    for(index in list){
      lview = view.factory('chat/list');
      lview.set('user', list[index]);
      stream.listen(node.render(lview));
    }
    stream.apply(false, function(list){
      form.set('list', list);
    });
  })

  return node.render(form);
})
.listen(Model.register('Socket'))
.listen(Model.register('Render/Dom', $content))
.then(function(data, socket, dom){
  dom.render(data);
  var $message = dom.search('#message'),
      $send = dom.search('#send');

  view.asEventStream('message').send(socket);
  
  $send.onclick = function(e){
    e.preventDefault();
    view.set('message', $message.value);
  };
})
.fail(function(error){
  console.log(error);
});
