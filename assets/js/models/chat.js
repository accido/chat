define('app/models/chat', function(){
  return function(model){
    var base                                  = null;

    model.prototype.vars                      = {
      VAR_CHAT                                : null,
      VAR_LIST                                : null,
    }

    model.prototype.init                      = function(){
      base                                    = this;
      this.register('Socket').then(function(socket){
        base.asEventStream('VAR_CHAT').alias('chat').recv(socket).then(function(data){
          //console.log('chat', data)
        })
        base.asEventStream('VAR_LIST').alias('list').recv(socket).then(function(data){
          //console.log('list', data)
        })
      });
    }
  }
})
