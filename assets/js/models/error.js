define('app/models/error', ['view'], function(View){
  return function(model){
    var base                                          = null

    model.prototype.vars                              = {
      VAR_ERROR                                       : null
    }

    model.prototype.init                              = function(){
      var errorEl                                          = document.createElement('div')
      document.body.appendChild(errorEl)
      base                                            = this
      this.register('Socket').then(function(socket){
        base.asEventStream('VAR_ERROR')
        .alias('error')
        .recv(socket)
        .then(function(error){

          if(error.type && 'system' === error.type){
            base.register('Node/Dependency', document.body, 'error').then(function(dep){
              return base.register('Render/Node', dep)
            })
            .then(function(node){
              var view                                  = View.factory('error')
                
              view.set('content', error.value)
              view.set('title', 'Error')

              return node.render(view)
            })
            .listen(base.register('Render/DOM', errorEl))
            .then(function(data, dom){
              dom.render(data)
              var $button = dom.search('.close-button')
              $button.onclick = function(e){
                e.preventDefault()
                errorEl.innerHTML = ''
              }
            })
            return
          }
          
          var el                                      = document.querySelector('.error-' + error.field)
          el.innerHTML                                = error.value
          el.style.display                            = error.value ? 'block' : 'none'

        })
      })
    }
  }
})
