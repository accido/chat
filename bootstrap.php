<?php
namespace Accido;
use Accido\Models\Application;
use Accido\Models\Request;
use Accido\Models\Response;
use Accido\Models\Response\Http\Header;
use Accido\Controller;
use Accido\Model;
use Accido\View;
use Accido\Exceptions\Option as OptionError;
use Accido\Loader;
use Exception;
defined('CORE_ROOT') or die('No direct script access.');
ini_set('error_reporting', 'stdout');ini_set('display_errors',1);error_reporting(E_ALL & ~E_WARNING & ~E_STRICT);
$app                                                  = new Application();
$app->handle('application/run')
  ->then($app->register('Action/Init/Shutdown'))
  ->then($app->register('Action/Init/Logger'))
  //->then($app->register('Async'))
  ->then($app->register('Action/Init/Server'));

$app->handle('/')
  ->then($app->register('Render/Socket/Relate'))
  ->then(function(Controller $ctrl) use ($app) {
    $ctrl->view                                       = new View('chat');
    $ctrl->view->base_url                             = $ctrl->request[Request::OPT_BASE_URL];
    $ctrl->commit()->then(function(Controller $ctrl) use ($app) {
      $ctrl->response->http([
        Header::CONTENT_TYPE                          => 'text/html',
        Header::CONNECTION                            => 'keep-alive',
        Header::SERVER                                => Header::ATTR_DEFAULT_SERVER,
      ]);

      $loader                                         = new Loader($app);
      $identity                                       = $loader->model('Identity', $ctrl, $app);
      return $ctrl;
    });
    return $ctrl;
  })
  ->then($app->register('Render/Socket/Header'))
  ->then($app->register('Render/Socket/Send'))
  ->then($app->register('Action/Socket/Disconnect'));

$app->handle('assets/(core|js|css|img)/.*')
  ->then($app->register('Render/Direct', [
    '#assets/core/.*#'                                => PROJECT_ROOT . '/core/public_html/',
    '#assets/(?:js|css|img)/.*#'                      => PROJECT_ROOT,
  ]))
  ->then($app->register('Render/Mime'))
  ->then($app->register('Render/Socket/Header'))
  ->then($app->register('Render/Socket/Reader'))
  ->then($app->register('Action/Socket/Disconnect'));

