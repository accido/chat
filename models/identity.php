<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Injector;
use Accido\Models\Application;
use Accido\Controller;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: User
 *
 * @package Chat
 * @subpackage Model
 * 
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Identity extends Model {
  const OPT_LOGIN                     = 'login';
  const OPT_PASSWORD                  = 'password';
  const OPT_ERROR                     = 'error';
  const OPT_LOGIN_SUBMIT              = 'login_submit';
  const OPT_REGISTER_LOGIN            = 'register_login';
  const OPT_REGISTER_PASSWORD         = 'register_password';
  const OPT_REGISTER_REPASSWORD       = 'register_repassword';
  const OPT_REGISTER_SUBMIT           = 'register_submit';
  const OPT_USER                      = 'user';

  protected $vars                     = [
    self::OPT_LOGIN                   => null,
    self::OPT_PASSWORD                => null,
    self::OPT_ERROR                   => null,
    self::OPT_LOGIN_SUBMIT            => null,
    self::OPT_REGISTER_LOGIN          => null,
    self::OPT_REGISTER_PASSWORD       => null,
    self::OPT_REGISTER_REPASSWORD     => null,
    self::OPT_REGISTER_SUBMIT         => null,
    self::OPT_USER                    => null,
  ];

  public function capture(Injector $dic, Controller $ctrl, Application $app){
    $this->stream(self::OPT_ERROR)->send($ctrl);

    $this->stream(self::OPT_LOGIN)
      ->recv($ctrl)
      ->then(function($login) use($app){
        if(!$app->checkLogin($login)){
          $this[self::OPT_ERROR]      = [
            'field'                   => 'login',
            'value'                   => 'Такого пользователя не существует',
          ];
        }
        else{
          $this[self::OPT_ERROR]      = [
            'field'                   => 'login',
            'value'                   => '',
          ];
        }
      });

    $this->stream(self::OPT_PASSWORD)
      ->recv($ctrl)
      ->then(function($pass) use($app){
        if(!($r = $app->checkPassword($this[self::OPT_LOGIN], $pass))){
          $this[self::OPT_ERROR]      = [
            'field'                   => 'password',
            'value'                   => 'Неверный пароль',
          ];
        }
        else{
          $this[self::OPT_ERROR]      = [
            'field'                   => 'password',
            'value'                   => '',
          ];
        }
      });

    $this->stream(self::OPT_REGISTER_LOGIN)
      ->alias('rlogin')
      ->recv($ctrl)
      ->then(function($login) use($app){
        if(!preg_match('/^[\pN\pL]{3,}$/u', $login)){
          $this[self::OPT_ERROR]      = [
            'field'                   => 'login',
            'value'                   => 'Неверный логин(допустимы только буквы и цифры)',
          ];
        }
        else{
          $this[self::OPT_ERROR]      = [
            'field'                   => 'login',
            'value'                   => '',
          ];
        }
      });

    $this->stream(self::OPT_REGISTER_PASSWORD)
      ->alias('rpassword')
      ->recv($ctrl)
      ->then(function($pass) use($app){
        if(3 > strlen($pass)){
          $this[self::OPT_ERROR]      = [
            'field'                   => 'password',
            'value'                   => 'Пароль слишком короткий',
          ];
        }
        else{
          $this[self::OPT_ERROR]      = [
            'field'                   => 'password',
            'value'                   => '',
          ];
        }
      });

    $this->stream(self::OPT_REGISTER_REPASSWORD)
      ->alias('repassword')
      ->recv($ctrl)
      ->then(function($pass) use($app){
        if($this[self::OPT_REGISTER_PASSWORD] !== $pass){
          $this[self::OPT_ERROR]      = [
            'field'                   => 'repassword',
            'value'                   => 'Пароли не совпадают',
          ];
        }
        else{
          $this[self::OPT_ERROR]      = [
            'field'                   => 'repassword',
            'value'                   => '',
          ];
        }
      });

    $this->stream(self::OPT_REGISTER_SUBMIT)
      ->alias('rsubmit')
      ->recv($ctrl)
      ->iterate()
      ->then(function($login, $pass) use($app, $ctrl){
        $app->registerUser($login, $pass, $ctrl);
      });

    $this->stream(self::OPT_LOGIN_SUBMIT)
      ->alias('submit')
      ->recv($ctrl)
      ->iterate()
      ->then(function($login, $pass) use($app, $ctrl){
        $user = $app->login($login, $pass, $ctrl);
        $this->user = $user->login;
      });

    $this->stream(self::OPT_USER)->alias('user')->send($ctrl);
  }
}
