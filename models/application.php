<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Controller;
use Accido\Loader;
use Accido\Models\User;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Application
 *
 * @package Application
 * @subpackage Model
 * 
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Application extends Model{

  const OPT_TRANSACTION_REGISTER                  = 'tr_register';
  const OPT_TRANSACTION_LOGIN                     = 'tr_login';
  const OPT_TRANSACTION_MESSAGE                   = 'tr_message';
  const OPT_USER                                  = 'user';
  const OPT_LIST                                  = 'list';
  const OPT_TEXT                                  = 'text';

  protected $vars                                 = [
    self::OPT_USER                                => null,
    self::OPT_TRANSACTION_REGISTER                => null,
    self::OPT_TRANSACTION_LOGIN                   => null,
    self::OPT_TRANSACTION_MESSAGE                 => null,
    self::OPT_LIST                                => null,
    self::OPT_TEXT                                => null,
  ];

  protected function init(){
    $loader                                       = new Loader(self::$dic);
    $tr                                           = $loader->model('Transaction', 'register');
    $this[self::OPT_TRANSACTION_REGISTER]         = $tr;
    $user                                         = &$this->ref(self::OPT_USER);
    $tr->bind(function($login, $pass, $session) use(&$user){
      $loader                                     = new Loader($this);
      $user[$login]                               = $loader->model('User', $login, $pass, $session);
    });

    $tl                                           = $loader->model('Transaction', 'login');
    $this[self::OPT_TRANSACTION_LOGIN]            = $tl;
    $list                                         = &$this->ref(self::OPT_LIST);
    $tl->bind(function($login) use(&$list){
      $list[$login]                               = $login;
      $this->trigger(self::OPT_LIST);
    });

    $tm                                           = $loader->model('Transaction', 'message');
    $this[self::OPT_TRANSACTION_MESSAGE]          = $tm;
    $text                                         = &$this->ref(self::OPT_TEXT);
    $tm->bind(function($user, $message) use(&$text){
      $text[]                                     = [
        'name'                                    => $user,
        'message'                                 => $message,
      ];
      $this->trigger(self::OPT_TEXT);
    });
  }

  /**
   * login
   * 
   * @param string $name
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function login($name, $pass, Controller $ctrl){
    $loader                                       = new Loader($this);
    $helper                                       = $loader->instance('Socket/Helper', $ctrl);
    $helper->session($this[self::OPT_USER][$name]->session);
    $tl                                           = $this[self::OPT_TRANSACTION_LOGIN];
    $tl->run($name);
    $this->stream(self::OPT_LIST)->alias('list')->send($ctrl);
    $this->stream(self::OPT_TEXT)->alias('chat')->send($ctrl);
    $user                                         = $this[self::OPT_USER][$name];
    $user->stream(User::OPT_MESSAGE)->alias('message')->recv($ctrl);
  }

  /**
   * message
   * 
   * @param string $data
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function message($login, $data){
  }

  /**
   * 
   * @param mixed $login
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function checkLogin($login){
    $user                                         = $this[self::OPT_USER];
    return isset($user[$login]);
  }

  /**
   * 
   * @param mixed $login
   * @param mixed $pass
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function checkPassword($login, $pass){
    $user                                         = $this[self::OPT_USER];
    return isset($user[$login]) && $user[$login]->checkPassword($pass);
  }

  /**
   * 
   * @param mixed $login
   * @param mixed $pass
   * @param Controller $session
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function registerUser($login, $pass, Controller $ctrl){
    $tr                                           = $this[self::OPT_TRANSACTION_REGISTER];
    $loader                                       = new Loader($this);
    $helper                                       = $loader->instance('Socket/Helper', $ctrl);
    //$tr->run($login, $pass, $helper->refresh());
    $tr->run($login, $pass, $helper->session());
  }
}
