<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Injector;
use Accido\Models\Socket;
use Accido\Models\Application;
use Accido\Controller;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: User
 *
 * @package Chat
 * @subpackage Model
 * 
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class User extends Model {
  const OPT_LOGIN                     = 'login';
  const OPT_PASSWORD                  = 'password';
  const OPT_SESSION                   = 'session';
  const OPT_MESSAGE                   = 'message';

  protected $vars                     = [
    self::OPT_LOGIN                   => null,
    self::OPT_PASSWORD                => null,
    self::OPT_SESSION                 => null,
    self::OPT_MESSAGE                 => null,
  ];

  public function capture(Application $app, $login, $pass, $session){
    $this[self::OPT_LOGIN]            = $login;
    $this[self::OPT_PASSWORD]         = $pass;
    $this[self::OPT_SESSION]          = $session;
    $this->stream(self::OPT_MESSAGE)->then(function($message) use($app) {
      $tm                             = $app[Application::OPT_TRANSACTION_MESSAGE];
      $tm->run($this[self::OPT_LOGIN], $message);
    });
  }

  /**
   * 
   * @param mixed $pass
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function checkPassword($pass){
    return $this[self::OPT_PASSWORD] === $pass;
  }


}
